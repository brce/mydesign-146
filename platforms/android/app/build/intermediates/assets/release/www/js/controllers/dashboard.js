angular.module('mydesignApp')
.controller('dashboardCtrl', ['$window', '$rootScope', '$scope', '$http', '$location', function($window, $rootScope, $scope, $http, $location) {
  
  $scope.back = function() { 
    window.history.back();
 };

 $scope.logout = function () {
    $window.localStorage.removeItem('userInfo');
    $location.path('home');
};

    $scope.subscription = function(user) {
        $http.post(__env.apiUrl + '/subscription', $rootScope.user)
        .success(function(data, status, headers, config) {
            $scope.success = true;
            $scope.message = 'Thank you!';
            $('#confirmation-window').show(); 
        })
        .error(function(data, status, headers, config) {
            $scope.showError = true;
            $scope.message = 'Oops! Something went wrong!';
            $('#confirmation-window').show(); 
        });
    };
  
  $scope.chooseShape = function(){
      if (!$rootScope.user.logged && $rootScope.notlogged === undefined) {
        $rootScope.notlogged = 1;
        $location.path('withoutlogin');
      } else {
        $location.path('chooseshape');
      }
  }

  $scope.invite = function(email){
      var inviteObj = {
          friend: $rootScope.user.logged ? $rootScope.user.email : 'Your friend',
          first: email.first,
          second: email.second,
          third: email.third
        };

      $http.post(__env.apiUrl + '/invite', inviteObj)
      .success(function(data, status, headers, config) {
        $scope.thanks = true;
        $scope.message = 'Thank you for sharing!';
        $('#confirmation-window').show(); 
          setTimeout($location.path('dashboard'), 500);
      })
      .error(function(data, status, headers, config) {
          $scope.showError = true;
          $scope.message = 'Oops! Something went wrong!';
          $('#confirmation-window').show(); 
      });
    }

    $scope.password_recover = function(user){
      $http.post( __env.apiUrl + '/reset', user)
      .success(function(data, status, headers, config) {
          $location.path('signin');
      })
      .error(function(data, status, headers, config) {
          $scope.showError = true;
          $scope.message = 'Oops! Something went wrong!';
          $('#confirmation-window').show(); 
      });
    }
}]);
