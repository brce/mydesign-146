/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.pinkmartini.mydesign;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String APPLICATION_ID = "com.pinkmartini.mydesign";
  public static final String BUILD_TYPE = "release";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 10608;
  public static final String VERSION_NAME = "1.6.8";
}
