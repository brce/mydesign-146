angular.module('mydesignApp')
.controller('categoryCtrl', ['$window', '$rootScope', '$scope', '$http', '$location', function ($window, $rootScope, $scope, $http, $location) {
    $scope.back = function() { 
        window.history.back();
     };

     $scope.logout = function () {
        $window.localStorage.removeItem('userInfo');
        $location.path('home');
    };

     if ($location.path() === '/choosecategory') {  
        $scope.img_path = __env.apiUrl + '/../static/';     
        $http.get(__env.apiUrl + '/categories')
        .success(function(data, status, headers, config) {
            $scope.categories = data;
        })
        .error(function(data, status, headers, config) {
            $scope.showError = true;
            $scope.message = 'Oops! Something went wrong!';
            $('#confirmation-window').show(); 
        });
     }
    
    $scope.chooseCategory = function(category){
        $rootScope.category = category;
        $rootScope.occasion = { id :0 };
        if (category.name === "Occassions") {
            $location.path('/chooseoccassion');
        }
        $location.path('/chooseclothes');
    };

}]);
