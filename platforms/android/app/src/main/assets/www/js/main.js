function sizes() {
    $('.wrapper').css('height', $(window).height());
    $('.wrapper').css('width', $(window).width());
    if (screen.width < 767) {
        document.getElementById("viewport").setAttribute("content", "initial-scale=1, maximum-scale=1, user-scalable=no");
    }
    if (screen.width > 767) {
        document.getElementById("viewport").setAttribute("content", "initial-scale=1.5, maximum-scale=1.5, user-scalable=no");
    }
    if (screen.width >= 1024) {
        document.getElementById("viewport").setAttribute("content", "initial-scale=2, maximum-scale=2, user-scalable=no");
    }
}

function general() {
    $('.wrapper').css('height', $(window).height());
    $('.user-menu-pill').click(function () {
        $('.user-menu').addClass('user-menu-open');
    });
    $('.user-menu-close').click(function () {
        $('.user-menu').removeClass('user-menu-open');
    });
    $('input[name=measure]').click(function () {
        $('.size-confirm').fadeIn(300);
    });
    $(".app_card.mmyy").keyup(function() {
        var maxChars = 2;
        if ($(this).val().length > maxChars) {
            $(this).val($(this).val().substr(0, maxChars));
        }
    });
    $(".app_card.cvs").keyup(function() {
        var maxChars = 6;
        if ($(this).val().length > maxChars) {
            $(this).val($(this).val().substr(0, maxChars));
        }
    });
    $(".app_card.cardNumber").keyup(function() {
        var maxChars = 16;
        if ($(this).val().length > maxChars) {
            $(this).val($(this).val().substr(0, maxChars));
        }
    });
}

setInterval(function () {
    var clotheHeight = $('.fabric_slider').css('height');
    $('.clothe-img').css('height', clotheHeight);
}, 100);

setInterval(function () {
    var clotheMargin = $('.clothe-img').css('width');
    var clotheMargin2 = parseInt(clotheMargin, 10) / -2;
    $('.clothe-img').css('margin-left', clotheMargin2);
}, 100);


$(document).off('click', '.spin').on('click', '.spin', function(e) {
if ($(".clothe-img-1").hasClass('clothes-active')) {
            $(".clothe-img-1").removeClass('clothes-active')
            $(".clothe-img-2").addClass('clothes-active2')
        } else if ($(".clothe-img-2").hasClass('clothes-active2')) {
            $(".clothe-img-2").removeClass('clothes-active2')
            $(".clothe-img-3").addClass('clothes-active3')
        } else if ($(".clothe-img-3").hasClass('clothes-active3')) {
            $(".clothe-img-3").removeClass('clothes-active3')
            $(".clothe-img-4").addClass('clothes-active4')
        } else if ($(".clothe-img-4").hasClass('clothes-active4')) {
            $(".clothe-img-4").removeClass('clothes-active4')
            $(".clothe-img-1").addClass('clothes-active')
        }
                });


setInterval(function () {
    if ($('.item-2').parent().hasClass('active')) {
        $('.btn-footer').css('z-index', '9999');

    } else if ($('.item-3').parent().hasClass('active')) {
        $('.btn-footer').css('z-index', '9999');

    } else {
        $('.btn-footer').css('z-index', '0');
    }
}, 100);

$(document).ready(function () {
    $('.marble.subscribe-accordion').click(function () {
        $(this).parent().find('.subscribe-accordion-content').slideToggle("slow");
    });
});

$(document).ready(function () {
    $('.marble.subscribe-accordion').click(function () {
        $('.more-subscribe').toggleClass('active');
    });
});

$(document).ready(function () {
    $("button[rel=invite-modal]").click(function (ev) {
        ev.preventDefault();
        var id = $(this).attr("href");

        var alturaTela = $(document).height();
        var larguraTela = $(window).width();


        $('#invite-mask').css({
            'width': larguraTela,
            'height': alturaTela
        });
        $('#invite-mask').fadeIn(1000);
        $('#invite-mask').fadeTo("slow", 0.8);

        $(id).show();
    });

    $("#invite-mask").click(function () {
        $(this).hide();
        $(".invite-window").hide();
    });

    $('.invite-close').click(function (ev) {
        ev.preventDefault();
        $("#invite-mask").hide();
        $(".invite-window").hide();
    });
});

/*
setInterval(function () {
    if( $('select[name=delivery-select-country] option:selected').val() == 'canada' ) {
        $('.delivery-select-usa').hide();
        $('.delivery-select-canada').show();
    }
    else if( $('select[name=delivery-select-country] option:selected').val() == 'usa' ) {
        $('.delivery-select-canada').hide();
        $('.delivery-select-usa').show();
    }
}, 100);

setInterval(function () {
    if( $('select[name=billing-select-country] option:selected').val() == 'canada' ) {
        $('.billing-select-usa').hide();
        $('.billing-select-canada').show();
    }
    else if( $('select[name=billing-select-country] option:selected').val() == 'usa' ) {
        $('.billing-select-canada').hide();
        $('.billing-select-usa').show();
    }
}, 100);
*/

setInterval(function () {
    var move1 = $('.clothe-align-1').width();
    var movedivided1 = parseInt(move1 / 2);
    $('.clothe-align-1').css({'margin-left': -movedivided1})
}, 100);

setInterval(function () {
    var move2 = $('.clothe-align-2').width();
    var movedivided2 = parseInt(move2 / 2);
    $('.clothe-align-2').css({'margin-left': -movedivided2})
}, 100);

$(document).ready(function () {
    $("a[rel=share-modal]").click(function (ev) {
        ev.preventDefault();

        var id = $(this).attr("href");

        var alturaTela = $(document).height();
        var larguraTela = $(window).width();


        $('#share-mask').css({
            'width': larguraTela,
            'height': alturaTela
        });
        $('#share-mask').fadeIn(1000);
        $('#share-mask').fadeTo("slow", 0.8);

        $('#share-window').show();
    });

    $("#share-mask").click(function () {
        $(this).hide();
        $("#share-window").hide();
    });

    $('.share-close').click(function (ev) {
        ev.preventDefault();
        $("#share-mask").hide();
        $("#share-window").hide();
    });
});

$(document).ready(function () {
    $("a[rel=filter-modal]").click(function (ev) {
        ev.preventDefault();

        var id = $(this).attr("href");

        var alturaTela = $(document).height();
        var larguraTela = $(window).width();


        $('#filter-mask').css({
            'width': larguraTela,
            'height': alturaTela
        });
        $('#filter-mask').fadeIn(1000);
        $('#filter-mask').fadeTo("slow", 0.8);

        $('#filter-window').show();
    });

    $("#filter-mask").click(function () {
        $(this).hide();
        $("#filter-window").hide();
    });

    $('.filter-close').click(function (ev) {
        ev.preventDefault();
        $("#filter-mask").hide();
        $("#filter-window").hide();
    });
});

$(document).ready(function () {
    $(".confirmation-modal").click(function (ev) {
        ev.preventDefault();

        var id = $(this).attr("href");

        var alturaTela = $(document).height();
        var larguraTela = $(window).width();


        $('#confirmation-mask').css({
            'width': larguraTela,
            'height': alturaTela
        });
        $('#confirmation-mask').fadeIn(1000);
        $('#confirmation-mask').fadeTo("slow", 0.8);

        $('#confirmation-window').show();
    });

    $("#confirmation-mask").click(function () {
        $(this).hide();
        $("#confirmation-window").hide();
    });

    $('.confirmation-close').click(function (ev) {
        ev.preventDefault();
        $("#confirmation-mask").hide();
        $("#confirmation-window").hide();
    });

    $('.confirmation-no').click(function (ev) {
        ev.preventDefault();
        $("#confirmation-mask").hide();
        $("#confirmation-window").hide();
    });
});

$(document).ready(function () {
    $(".closet-modal").click(function (ev) {
        ev.preventDefault();

        var id = $(this).attr("href");

        var alturaTela = $(document).height();
        var larguraTela = $(window).width();


        $('#closet-mask').css({
            'width': larguraTela,
            'height': alturaTela
        });
        $('#closet-mask').fadeIn(1000);
        $('#closet-mask').fadeTo("slow", 0.8);

        $('#closet-window').show();
    });

    $("#closet-mask").click(function () {
        $(this).hide();
        $("#closet-window").hide();
    });

    $('.closet-close').click(function (ev) {
        ev.preventDefault();
        $("#closet-mask").hide();
        $("#closet-window").hide();
    });
});

$(document).ready(function () {
    $(".ch1-modal").click(function (ev) {
        ev.preventDefault();

        var id = $(this).attr("href");

        var alturaTela = $(document).height();
        var larguraTela = $(window).width();


        $('#ch1-mask').css({
            'width': larguraTela,
            'height': alturaTela
        });
        $('#ch1-mask').fadeIn(1000);
        $('#ch1-mask').fadeTo("slow", 0.8);

        $('#ch1-window').show();
    });

    $("#ch1-mask").click(function () {
        $(this).hide();
        $("#ch1-window").hide();
    });

    $('.ch1-close').click(function (ev) {
        ev.preventDefault();
        $("#ch1-mask").hide();
        $("#ch1-window").hide();
    });
});

$(document).ready(function () {
    $(".ch2-modal").click(function (ev) {
        ev.preventDefault();

        var id = $(this).attr("href");

        var alturaTela = $(document).height();
        var larguraTela = $(window).width();


        $('#ch2-mask').css({
            'width': larguraTela,
            'height': alturaTela
        });
        $('#ch2-mask').fadeIn(1000);
        $('#ch2-mask').fadeTo("slow", 0.8);

        $('#ch2-window').show();
    });

    $("#ch2-mask").click(function () {
        $(this).hide();
        $("#ch2-window").hide();
    });

    $('.ch2-close').click(function (ev) {
        ev.preventDefault();
        $("#ch2-mask").hide();
        $("#ch2-window").hide();
    });
});
$(document).ready(function () {
    $(".clickno-modal").click(function (ev) {
        ev.preventDefault();

        var id = $(this).attr("href");

        var alturaTela = $(document).height();
        var larguraTela = $(window).width();


        $('#clickno-mask').css({
            'width': larguraTela,
            'height': alturaTela
        });
        $('#clickno-mask').fadeIn(1000);
        $('#clickno-mask').fadeTo("slow", 0.8);

        $('#clickno-window').show();
    });

    $("#clickno-mask").click(function () {
        $(this).hide();
        $("#clickno-window").hide();
    });

    $('.clickno-close').click(function (ev) {
        ev.preventDefault();
        $("#clickno-mask").hide();
        $("#clickno-window").hide();
    });
});


$(document).ready(function () {
    $('.owl-home').owlCarousel({
        margin: 0,
        nav: false,
        loop: false,
        responsive: {
            0: {
                items: 1
            },
        }
    })
});

$('.wpaper').click(function () {
    if ($(this).is(':checked')) {
        $('.with-check').css('display', 'block');
        $('.without-check').css('display', 'none');
    } else {
        $('.with-check').css('display', 'none');
        $('.without-check').css('display', 'block');
    }
});
$('.not-a-gift').click(function () {
    if (!$(this).is(':checked')) {
        $('.gift-detail').fadeIn(200);
    } else {
        $('.gift-detail').fadeOut(200);    
    }
});

$('.measure-xs-input').click(function () {
    if ($(this).is(':checked')) {
        $('.measure-xs').addClass('size-selected');

        $('.measure-s').removeClass('size-selected');
        $('.measure-m').removeClass('size-selected');
        $('.measure-l').removeClass('size-selected');
        $('.measure-xl').removeClass('size-selected');
        $('.measure-2xl').removeClass('size-selected');
        $('.measure-3xl').removeClass('size-selected');
        $('.measure-4xl').removeClass('size-selected');
    }
});

$('.measure-s-input').click(function () {
    if ($(this).is(':checked')) {
        $('.measure-s').addClass('size-selected');

        $('.measure-xs').removeClass('size-selected');
        $('.measure-m').removeClass('size-selected');
        $('.measure-l').removeClass('size-selected');
        $('.measure-xl').removeClass('size-selected');
        $('.measure-2xl').removeClass('size-selected');
        $('.measure-3xl').removeClass('size-selected');
        $('.measure-4xl').removeClass('size-selected');
    }
});

$('.measure-m-input').click(function () {
    if ($(this).is(':checked')) {
        $('.measure-m').addClass('size-selected');

        $('.measure-xs').removeClass('size-selected');
        $('.measure-s').removeClass('size-selected');
        $('.measure-l').removeClass('size-selected');
        $('.measure-xl').removeClass('size-selected');
        $('.measure-2xl').removeClass('size-selected');
        $('.measure-3xl').removeClass('size-selected');
        $('.measure-4xl').removeClass('size-selected');
    }
});

$('.measure-l-input').click(function () {
    if ($(this).is(':checked')) {
        $('.measure-l').addClass('size-selected');

        $('.measure-xs').removeClass('size-selected');
        $('.measure-s').removeClass('size-selected');
        $('.measure-m').removeClass('size-selected');
        $('.measure-xl').removeClass('size-selected');
        $('.measure-2xl').removeClass('size-selected');
        $('.measure-3xl').removeClass('size-selected');
        $('.measure-4xl').removeClass('size-selected');
    }
});

$('.measure-xl-input').click(function () {
    if ($(this).is(':checked')) {
        $('.measure-xl').addClass('size-selected');

        $('.measure-xs').removeClass('size-selected');
        $('.measure-s').removeClass('size-selected');
        $('.measure-m').removeClass('size-selected');
        $('.measure-l').removeClass('size-selected');
        $('.measure-2xl').removeClass('size-selected');
        $('.measure-3xl').removeClass('size-selected');
        $('.measure-4xl').removeClass('size-selected');
    }
});

$('.measure-2xl-input').click(function () {
    if ($(this).is(':checked')) {
        $('.measure-2xl').addClass('size-selected');

        $('.measure-xs').removeClass('size-selected');
        $('.measure-s').removeClass('size-selected');
        $('.measure-m').removeClass('size-selected');
        $('.measure-l').removeClass('size-selected');
        $('.measure-xl').removeClass('size-selected');
        $('.measure-3xl').removeClass('size-selected');
        $('.measure-4xl').removeClass('size-selected');
    }
});

$('.measure-3xl-input').click(function () {
    if ($(this).is(':checked')) {
        $('.measure-3xl').addClass('size-selected');

        $('.measure-xs').removeClass('size-selected');
        $('.measure-s').removeClass('size-selected');
        $('.measure-m').removeClass('size-selected');
        $('.measure-l').removeClass('size-selected');
        $('.measure-xl').removeClass('size-selected');
        $('.measure-2xl').removeClass('size-selected');
        $('.measure-4xl').removeClass('size-selected');
    }
});

$('.measure-4xl-input').click(function () {
    if ($(this).is(':checked')) {
        $('.measure-4xl').addClass('size-selected');

        $('.measure-xs').removeClass('size-selected');
        $('.measure-s').removeClass('size-selected');
        $('.measure-m').removeClass('size-selected');
        $('.measure-l').removeClass('size-selected');
        $('.measure-xl').removeClass('size-selected');
        $('.measure-2xl').removeClass('size-selected');
        $('.measure-3xl').removeClass('size-selected');
    }
});

$(document).ready(function () {
    $('.owl-carousel').owlCarousel({
        margin: 0,
        nav: false,
        loop: false,
        responsive: {
            0: {
                items: 1
            },
        }
    })
});


$(document).ready(function () {
    $('.owl-showroom').owlCarousel({
        margin: 0,
        autoplay: true,
        autoplayTimeout: 5000,
        nav: false,
        dots: false,
        loop: true,
        responsive: {
            0: {
                items: 1
            },
        }
    })
});

$('.same-address').click(function () {
    if ($(this)) {
        $('.not-same').addClass('address-show');
    }
});

$('.same-address').click(function () {
    if ($(this).is(':checked')) {
        $('.not-same').removeClass('address-show');
    }
});

$(document).ready(function () {
    general();
    sizes();
});

$(window).resize(function () {
    general();
    sizes();
})

setInterval(function () {
    sizes();
}, 100);