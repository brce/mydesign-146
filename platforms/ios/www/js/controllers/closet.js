angular.module('mydesignApp')
.controller('closetCtrl', ['$window', '$rootScope', '$scope', '$http', '$location', function ($window, $rootScope, $scope, $http, $location) {
    $scope.back = function() { 
        window.history.back();
    };

    $scope.logout = function () {
        $window.localStorage.removeItem('userInfo');
        $location.path('home');
    };
    
    $http.get(__env.apiUrl + '/closet/user/' + $rootScope.user.id)
    .success(function(data, status, headers, config) {
        $('.loading').fadeIn(300);
        $rootScope.img_path = __env.apiUrl + '/../static/';
        $rootScope.closet = data.filter(function (x) { return !!x });
        $('.loading').fadeOut(300);
        if ($rootScope.closet.legth === 0) {
            $scope.showError = true;
            $scope.message = 'Your closet is empty!';
            $('#confirmation-window').show(); 
        }
    })
    .error(function(data, status, headers, config) {
        $('.loading').fadeOut(300);
        $scope.showError = true;
        $scope.message = 'Your closet is empty!';
        $('#confirmation-window').show(); 
    });

    $scope.measure = function(clothes){
        $rootScope.model = clothes;
        $location.path('/sizechart');
    };

    $scope.shareDesign = function(clothes){
        $rootScope.model = clothes;
        $rootScope.fb_link = 'http://ca.mydesignclothes.com:5000/v1/share/' + $rootScope.model.image_front.split(/[/]+/).pop();
        $rootScope.tw_link = "https://twitter.com/intent/tweet?text=" + "Hey there, I'm using My Design App to create amazing custom clothes for me." +
        "&url=http://mydesignclothes.com" + "&image="  + __env.apiUrl + '/../static/' + $rootScope.model.image_front;
        $rootScope.social_link = 'http://ca.mydesignclothes.com:5000/v1/share/' + $rootScope.model.image_front.split(/[/]+/).pop();
        $location.path('/sharedesign');
    };

    $scope.orderBy = function(type){
        var filter = ['clothing_id', 'cart_id'];
        var unsorted = $rootScope.closet;
        $rootScope.closet = unsorted.sort(function (a, b) {
            return a[filter[type]] - b[filter[type]];
        });
        $("#filter-mask").hide();
        $("#filter-window").hide();
    };

    $scope.addToCart = function(clothes){
        $rootScope.model = clothes;
        if ($rootScope.model.id !== null && $rootScope.model.id !== undefined && $rootScope.model.chest !== null) {
            $location.path('/confirmadjust');
        } else {
            $location.path('/adjustsize'); 
        }        
    };

    $scope.delete = function(clothes){
        $http.post(__env.apiUrl + '/closet/clothes/' + $rootScope.model.id)
        .success(function(data, status, headers, config) {
            var index = $rootScope.closet.indexOf(clothes);
            $rootScope.closet.splice(index, 1);  
            $location.path('/closet');   
        })
        .error(function(data, status, headers, config) {
            $scope.showError = true;
            $scope.message = 'Oops! Something went wrong!';
            $('#confirmation-window').show(); 
        });
    };
}]);
