angular.module('mydesignApp')
.controller('usersCtrl', ['$window', '$rootScope', '$scope', '$http', '$location', 
    function ($window, $rootScope, $scope, $http, $location) {

    $scope.back = function() { 
      window.history.back();
    }; 

    if ($rootScope.user === undefined || $rootScope.user === null) {
        $rootScope.user = {};
    }

    $scope.province = [
        { code: "AB", name: "Alberta" },
        { code: "BC", name: "British Columbia" },
        { code: "MB", name: "Manitoba" },
        { code: "NB", name: "New Brunswick" },
        { code: "NL", name: "Newfoundland and Labrador" },
        { code: "NS", name: "Nova Scotia" },
        { code: "ON", name: "Ontario" },
        { code: "PE", name: "Prince Edward Island" },
        { code: "QC", name: "Quebec" },
        { code: "SK", name: "Saskatchewan" },
        { code: "NT", name: "Northwest Territories" },
        { code: "NU", name: "Nunavut" },
        { code: "YT", name: "Yukon" }
      ];

    function isUploadSupported() {
        if (navigator.userAgent.match(/(Android (1.0|1.1|1.5|1.6|2.0|2.1))|(Windows Phone (OS 7|8.0))|(XBLWP)|(ZuneWP)|(w(eb)?OSBrowser)|(webOS)|(Kindle\/(1.0|2.0|2.5|3.0))/)) {
            return false;
        }
        var elem = document.createElement('input');
        elem.type = 'file';
        return !elem.disabled;
    };

    if (window.File && window.FileReader && window.FormData) {
        var $inputField = $('#file');
    
        $inputField.on('change', function (e) {
            var file = e.target.files[0];
    
            if (file) {
                if (/^image\//i.test(file.type)) {
                    readFile(file);
                } else {
                    alert('Not a valid image!');
                }
            }
        });
    } else {
        alert("File upload is not supported!");
    }

    function readFile(file) {
        var reader = new FileReader();
    
        reader.onloadend = function () {
            processFile(reader.result, file.type);
        }
    
        reader.onerror = function () {
            alert('There was an error reading the file!');
        }
    
        reader.readAsDataURL(file);
    }

    function processFile(dataURL, fileType) {
        var maxWidth = 800;
        var maxHeight = 800;

        var image = new Image();
        image.src = dataURL;

        image.onload = function () {
            var width = image.width;
            var height = image.height;
            var shouldResize = (width > maxWidth) || (height > maxHeight);

            if (!shouldResize) {
                sendFile(dataURL);
                return;
            }

            var newWidth;
            var newHeight;

            if (width > height) {
                newHeight = height * (maxWidth / width);
                newWidth = maxWidth;
            } else {
                newWidth = width * (maxHeight / height);
                newHeight = maxHeight;
            }

            var canvas = document.createElement('canvas');

            canvas.width = newWidth;
            canvas.height = newHeight;

            var context = canvas.getContext('2d');

            context.drawImage(this, 0, 0, newWidth, newHeight);

            dataURL = canvas.toDataURL(fileType);

            sendFile(dataURL);
        };

        image.onerror = function () {
            alert('There was an error processing your file!');
        };
    }

    function sendFile(fileData) {
        var userObj = {
            id: $rootScope.user.id,
            profile_pic: fileData,
            logged: true
          };
  
          $http.post(__env.apiUrl + '/uploadphoto', userObj)
          .success(function(data, status, headers, config) {
            $rootScope.user.profile_pic = fileData;
            $window.localStorage.setItem('userInfo', JSON.stringify($rootScope.user));
          })
          .error(function(data, status, headers, config) {
              $scope.showError = true;
              $scope.message = 'Oops! Something went wrong!';
              $('#confirmation-window').show(); 
          });
    }

    if ($location.path() === '/editprofile' && $rootScope.address === undefined 
            && $rootScope.user !== undefined && /^\d+$/.test($rootScope.user.id)) {
        $http.get(__env.apiUrl + '/address/' + $rootScope.user.id)
        .success(function(data, status, headers, config) {
            $rootScope.address = data !== "null" ? data : {};
        })
        .error(function(data, status, headers, config) {
            $scope.showError = true;
            $scope.message = 'Oops! Something went wrong!';
            $('#confirmation-window').show(); 
        });
    }

    $scope.showPassword = function () {
        var n = document.getElementById("newpwd");
        var c = document.getElementById("cnewpwd");
        if (n.type === "password" || c.type === "password") {
            c.type = "text";
            n.type = "text";
        } else {
            c.type = "password";
            n.type = "password";
        }
    }

    function saveAddress(userObj) {
      var endpoint = '/address'
      if($rootScope.address.id !== null && $rootScope.address.id !== undefined) {
        endpoint = '/edit_address'
        userObj.address_id = $rootScope.address.id;
      }

        $http.post(__env.apiUrl + endpoint, userObj)
        .success(function(data, status, headers, config) {
            $rootScope.user = userObj;
            $scope.success = true;
            $scope.message = 'Updated successfully!';
            $('#confirmation-window').show(); 
        })
        .error(function(data, status, headers, config) {
            $scope.showError = true;
            $scope.message = 'Oops! Something went wrong!';
            $('#confirmation-window').show(); 
        });
    }

    $scope.edit = function(user, address){
      var userObj = {
          id: $rootScope.user.id,
          first_name: user.first_name,
          last_name: user.last_name,
          phone: user.phone,
          address: address.address,
          city: address.city,
          postal_code: address.postal_code,
          province: address.province,
          country: address.country,
          profile_pic: user.profile_pic,
          logged: true
        };

        $http.post(__env.apiUrl + '/edit_user', userObj)
        .success(function(data, status, headers, config) {
            saveAddress(userObj);
        })
        .error(function(data, status, headers, config) {
            $scope.showError = true;
            $scope.message = 'Oops! Something went wrong!';
            $('#confirmation-window').show(); 
        });
    }

    $scope.passwordRecover = function(user){
        $http.post(__env.apiUrl + '/reset', $rootScope.user)
        .success(function(data, status, headers, config) {
            $window.localStorage.setItem('userInfo', JSON.stringify(data.payload));
            $location.path('passwordsent');
        })
        .error(function(data, status, headers, config) {
			$scope.showError = true;
            $scope.message = 'Oops! Something went wrong!';
            $('#confirmation-window').show(); 
		});
    }

    $scope.changePassword = function(user){
        $http.post(__env.apiUrl + '/changepwd', $rootScope.user)
        .success(function(data, status, headers, config) {
            $scope.success = true;
            $scope.message = 'Changed successfully!';
            $('#confirmation-window').show(); 
        })
        .error(function(data, status, headers, config) {
			$scope.showError = true;
            $scope.message = 'Oops! Something went wrong!';
            $('#confirmation-window').show(); 
		});
    }

    $scope.logout = function () {
        $window.localStorage.removeItem('userInfo');
        $location.path('home');
    };

    $rootScope.fb_share = 'http://download.mydesignclothes.com:5000/v1/share/mydesignapp_share.png';

}]);
