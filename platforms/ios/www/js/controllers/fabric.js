angular.module('mydesignApp')
.controller('fabricCtrl', ['$window', '$rootScope', '$scope', '$http', '$location', function ($window, $rootScope, $scope, $http, $location) {
    
    $scope.back = function() { 
        if ($location.path() === '/confirmfabric') {
            $location.path('choosefabric');
        } else if ($location.path() === '/choosefabric') {
            $location.path('listfabric');
        } else if ($location.path() === '/listfabric') {
            $location.path('chooseclothes');
        } else {
            window.history.back();
        }
    };

    $scope.logout = function () {
        $window.localStorage.removeItem('userInfo');
        $location.path('home');
    };

    if($rootScope.clothes !== null) {
        $scope.clothes = $rootScope.clothes;
    }
    
    if($rootScope.model !== null) {
        $scope.model = $rootScope.model;
    }
    
    $scope.listFabrics = function(type){
        $('.loading').fadeIn(300);
        $http.get(__env.apiUrl + '/fabrics/' + type)
        .success(function(data, status, headers, config) {
            $rootScope.img_path = __env.apiUrl + '/../static/';
            $rootScope.fabricType = type;
            $rootScope.fabrics = data.filter(function(obj) {
                return JSON.parse("[" + $rootScope.clothes.fabric + "]").indexOf(obj.id) >= 0;
            });
            $('.loading').fadeOut(300);
            $location.path('/choosefabric');
        })
        .error(function(data, status, headers, config) {
            $('.loading').fadeOut(300);
            $scope.showError = true;
            $scope.message = 'Oops! Something went wrong!';
            $('#confirmation-window').show(); 
        });
    }   

    $scope.confirmFabric = function(fabric){
        $('.loading').fadeIn(300);
        $rootScope.fabric = fabric;
        $('.loading').fadeOut(300);
        $location.path('/confirmfabric');
    };

    $scope.saveFabric = function(fabric){
        $('.loading').fadeIn(300);
        $http.post(__env.apiUrl + '/item/clothing/'+ $rootScope.clothes.id +
                    '/fabric/' + $rootScope.fabric.id + '/shape/' + $rootScope.user.shape)
        .success(function(data, status, headers, config) {
            $rootScope.img_path = __env.apiUrl + '/../static/';
            $rootScope.model =  JSON.parse(data.payload);
            $('.loading').fadeOut(300);
            $location.path('/readytomeasure');
        })
        .error(function(data, status, headers, config) {
            $('.loading').fadeOut(300);
            $scope.showError = true;
            $scope.message = 'Oops! Something went wrong!';
            $('#confirmation-window').show(); 
        });
    };   

    $scope.add_to_closet = function() {
        var itemObj = {
            "user_id": $rootScope.user.id,
            "item_id": $rootScope.model.id
          };
  
        $http.post(__env.apiUrl + '/add_to_closet', itemObj)
        .success(function(data, status, headers, config) {
            $scope.success = true;
            $scope.message = 'Added to closet!';
            $('#confirmation-window').show(); 
        })
        .error(function(data, status, headers, config) {
			$scope.showError = true;
            $scope.message = 'Oops! Something went wrong!';
            $('#confirmation-window').show(); 
		});
    }

    $scope.getFrontByShape = function(clothes){
        switch($rootScope.user.shape) {
            case 1:
                return clothes.hourglass_front
                break;
            case 2:
                return clothes.triangle_front
                break;
            case 3:
                return clothes.retangle_front
                break;  
            case 4:
                return clothes.inverted_triangle_front
                break;  
            case 5:
                return clothes.circle_front
                break;     
            default:
                return clothes.triangle_front
        }
    };

    $scope.getLeftByShape = function(clothes){
        switch($rootScope.user.shape) {
            case 1:
                return clothes.hourglass_left
                break;
            case 2:
                return clothes.triangle_left
                break;
            case 3:
                return clothes.retangle_left
                break;  
            case 4:
                return clothes.inverted_triangle_left
                break;  
            case 5:
                return clothes.circle_left
                break;     
            default:
                return clothes.triangle_left
        }
    };

    $scope.getBackByShape = function(clothes){
        switch($rootScope.user.shape) {
            case 1:
                return clothes.hourglass_back
                break;
            case 2:
                return clothes.triangle_back
                break;
            case 3:
                return clothes.retangle_back
                break;  
            case 4:
                return clothes.inverted_triangle_back
                break;  
            case 5:
                return clothes.circle_back
                break;     
            default:
                return clothes.triangle_back
        }
    };

    $scope.getRightByShape = function(clothes){
        switch($rootScope.user.shape) {
            case 1:
                return clothes.hourglass_right
                break;
            case 2:
                return clothes.triangle_right
                break;
            case 3:
                return clothes.retangle_right
                break;  
            case 4:
                return clothes.inverted_triangle_right
                break;  
            case 5:
                return clothes.circle_right
                break;     
            default:
                return clothes.triangle_right
        }
    };

}]);
