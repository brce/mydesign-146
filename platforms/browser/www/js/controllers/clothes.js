angular.module('mydesignApp')
.controller('clothesCtrl', ['$window', '$rootScope', '$scope', '$http', '$location', function ($window, $rootScope, $scope, $http, $location) {
    $scope.back = function() { 
        window.history.back();
     };

     $scope.logout = function () {
        $window.localStorage.removeItem('userInfo');
        $location.path('home');
    };

     if ($location.path() === '/chooseclothes' || $location.path() === '/choosetshirts') {
        $scope.clothing = [];
        
        $http({
            url: __env.apiUrl + '/clothing/category/' + $rootScope.category.id +
                                '/occasion/' + $rootScope.occasion.id,
            method: "GET"
        }).success(function(result){
            $scope.clothing = result;
            $scope.img_path = __env.apiUrl + '/../static/';
            if ($scope.clothing.length === 0) {
                $scope.showError = true;
                $scope.message = 'Nothing found!';
                $('#confirmation-window').show(); 
            } 
        })
        .error(function(data, status, headers, config) {
            $scope.showError = true;
            $scope.message = 'Oops! Something went wrong!';
            $('#confirmation-window').show(); 
        });
     }
    
    $scope.chooseClothes = function(clothes){
        $('.loading').fadeIn(300);
        $rootScope.clothes = clothes;
        $('.loading').fadeOut(300);
        $location.path('/listfabric');
    };

    $scope.chooseShirt = function(clothes){
        $('.loading').fadeIn(300);
        $rootScope.clothes = clothes;
        $('.loading').fadeOut(300);
        $location.path('/listslogans');
    };

    $scope.getClothesByShape = function(clothes){
        switch($rootScope.user.shape) {
            case 1:
                return clothes.hourglass_sample
                break;
            case 2:
                return clothes.triangle_sample
                break;
            case 3:
                return clothes.retangle_sample
                break;  
            case 4:
                return clothes.inverted_triangle_sample
                break;  
            case 5:
                return clothes.circle_sample
                break;     
            default:
                return clothes.triangle_sample
        }
    };

}]);
