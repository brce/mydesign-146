(function (window) {
  window.__env = window.__env || {};

  // API url
  window.__env.apiUrl = 'http://ca.mydesignclothes.com:5000/v1';
  //window.__env.apiUrl = 'http://127.0.0.1:5000/v1';


  // Base url
  window.__env.baseUrl = '/';

  // Whether or not to enable debug mode
  // Setting this to false will disable console output
  window.__env.enableDebug = true;
}(this));
